/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.douglassilvadelima.view;

import br.com.douglassilvadelima.model.Cliente;
import br.com.douglassilvadelima.model.Conexao;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author usuario
 */
public class TelaLoginController implements Initializable {

    @FXML
    private Button entrarBT;
    @FXML
    private TextField loginTF;
    @FXML
    private TextField senhaTF;

    @FXML
    public void entrar(ActionEvent ev){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        String selectSQL = "SELECT * FROM ClienteTT3 WHERE login = ?";
        
        PreparedStatement ps;
        //if() verifica se é inteiro
            try{
                ps = dbConnection.prepareStatement(selectSQL);
                ps.setString(1, loginTF.getText());
            
                ResultSet rs = ps.executeQuery();
                if(rs.next()){
                    Cliente cliente = new Cliente();
                    cliente.setId(rs.getInt("id_cliente"));
                    cliente.setLogin(rs.getString("login"));
                    cliente.setSenha(rs.getString("senha"));                
                    if(cliente.getSenha().equals(senhaTF.getText())){
                        Trabalho3Trimestre.cliente = cliente;
                        Parent root;
                        try {
                            Stage stage = Trabalho3Trimestre.stage;
            
                            FXMLLoader loader = new FXMLLoader(getClass().getResource("TelaVendas.fxml"));
                            root = loader.load();
                            TelaVendasController telaVendas = loader.getController();
            
                            Scene scene = new Scene(root);
            
                            stage.setScene(scene);          
            
                        } catch (NullPointerException | IOException ex) {
                        System.out.println("Erro Arquivo FXML");
                        }
                    }else{
                    //msgSistema.setText("Senha ou usuário inválido");
                    }
                }
            }catch(SQLException e){
                e.printStackTrace();
            }
        }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        loginTF.setText("ManoEsqueleto");
        senhaTF.setText("123");
    }    
    
}
