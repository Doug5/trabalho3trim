/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.douglassilvadelima.view;

import br.com.douglassilvadelima.model.Cliente;
import br.com.douglassilvadelima.model.ObjetoCarrinho;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author Aluno
 */
public class Trabalho3Trimestre extends Application {
    public static Stage stage;
    public static Cliente cliente = null;
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("TelaLogin.fxml"));
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.setTitle("Vendas");
        stage.show();
        
        Trabalho3Trimestre.stage = stage;
        salvar();
    }
    
    private void salvar(){
    Trabalho3Trimestre.stage.setOnCloseRequest(new EventHandler<WindowEvent>(){
        @Override
        public void handle(WindowEvent event) {
            writeObject();
        }        
    });}

    /**
     * @param args the command line arguments
     */
    
    public void writeObject(){
     
       try{
        
        ArrayList<ObjetoCarrinho> aux = new ArrayList<>();
        if(TelaVendasController.objetosNoCarrinho != null){
            for(ObjetoCarrinho c: TelaVendasController.objetosNoCarrinho){
                aux.add(c);
            }
        
            FileOutputStream fout = new FileOutputStream("objetoscarrinho.ser");
         
            ObjectOutputStream oos = new ObjectOutputStream(fout);   

            oos.writeObject(aux);
         
            oos.close();
            System.out.println("Done");
        }
       }catch(Exception ex){
           ex.printStackTrace();
       } 
    }
    
    public static void main(String[] args) {
        launch(args);
    }
}

