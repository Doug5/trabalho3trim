/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.douglassilvadelima.view;

import br.com.douglassilvadelima.model.Produto;
import br.com.douglassilvadelima.model.Raca;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

public class FXMLDocumentController implements Initializable {

    @FXML
    private Label msgSistema;
    @FXML
    private TextField nomeTF;
    @FXML
    private TextField valorTF;
    @FXML
    private TextField estoqueTF;
    @FXML
    private ComboBox racaCB;
    @FXML
    private Button inserir;
    @FXML
    private Button addCarrinho;
    @FXML
    private Button finalizarCompra;
    @FXML
    private TableView<Produto> tabela;
    private ObservableList<Produto> produtos;
    @FXML
    private TableColumn<Produto, Integer> idCol;
    @FXML
    private TableColumn<Produto, String> nomeCol;
    @FXML
    private TableColumn<Produto, Double> valorCol;
    @FXML
    private TableColumn<Produto, Integer> estoqueCol;
    @FXML
    private TableColumn<Produto, String> racaCol;
    @FXML
    private ArrayList<Integer> carrinho = new ArrayList<>();
    
    private ObservableList<Raca> categorias;
    
    public void atualiza(){
        produtos = tabela.getItems(); 
        produtos.clear();        
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));      
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nome")); 
        valorCol.setCellValueFactory(new PropertyValueFactory<>("valor"));
        estoqueCol.setCellValueFactory(new PropertyValueFactory<>("estoque"));
        racaCol.setCellValueFactory(new PropertyValueFactory<>("raca"));
        
        ArrayList<Produto> produtosBanco = Produto.getAll();
        for(Produto p: produtosBanco){
            if(p.getEstoque()>0){
                    produtos.add(p);
            }
        }
        racaCB.getItems().clear();
        ArrayList<Raca> racas = Raca.getAll();
        racaCB.getItems().addAll(racas);

    }
   
    @FXML
    public void inserirProduto(ActionEvent ev){
        Produto p = new Produto();
        p.setNome(nomeTF.getText());
        p.setValor(Double.parseDouble(valorTF.getText()));
        p.setEstoque(Integer.parseInt(estoqueTF.getText()));
        p.setRaca((Raca) racaCB.getSelectionModel().getSelectedItem());
        p.inserir();
        atualiza();
    }
    @FXML
    public void addAoCarrinho(){
        carrinho.add(tabela.getSelectionModel().getSelectedItem().getId());
    }
    @FXML
    public void finalizarCompra(){
        for(Integer i: carrinho){
                Produto p = new Produto();
                p.setId(i);
                p.getOne();
                p.setEstoque(p.getEstoque()-1);
                p.update();
                atualiza();
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {     
        atualiza(); 
    }

}