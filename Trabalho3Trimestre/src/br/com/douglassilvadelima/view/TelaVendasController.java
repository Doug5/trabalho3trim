/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.douglassilvadelima.view;

import br.com.douglassilvadelima.model.Cliente;
import br.com.douglassilvadelima.model.Gravador;
import br.com.douglassilvadelima.model.ItemVenda;
import br.com.douglassilvadelima.model.ObjetoCarrinho;
import br.com.douglassilvadelima.model.Produto;
import br.com.douglassilvadelima.model.Raca;
import br.com.douglassilvadelima.model.Venda;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.ObjectInputStream;
import java.net.URL;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TelaVendasController implements Initializable {
    Gravador g = new Gravador();
    private int quantidadeDaCompra;
    @FXML
    private Label label;
    @FXML
    private TableView<ObjetoCarrinho> tabelaCarrinho;
    @FXML
    private TableColumn<ObjetoCarrinho, String> nomeCarrinhoCol;
    @FXML
    private TableColumn<ObjetoCarrinho, Double> valorCarrinhoCol;
    @FXML
    private TableColumn<ObjetoCarrinho, Integer> quantidadeCarrinhoCol;
    @FXML
    private TableColumn<ObjetoCarrinho, Double> valorTotalCarrinhoCol;
    @FXML
    private TextField quantidadeTF;
    @FXML
    private ComboBox produtoCB;
    @FXML
    private Label msgSistema;
    @FXML
    private Button addCarrinho;
    @FXML
    private Button finalizarCompra;
    @FXML
    private TableView<Venda> tabelaVendas;
    @FXML
    private TableColumn<Venda, Integer> idVendasCol;
    @FXML
    private TableColumn<Venda, Date> dataVendasCol;
    @FXML
    private TableColumn<Venda, String> formaPagamentoVendasCol;
    @FXML
    private TableColumn<Venda, String> clienteVendasCol;
    @FXML
    private TableColumn<Venda, Double> valorTotalVendasCol;
    @FXML
    private ToggleGroup formaPag;
    @FXML
    RadioButton boleto;
    @FXML
    RadioButton cartao;
    public static ObservableList<ObjetoCarrinho> objetosNoCarrinho = null;
    private ObservableList<Venda> vendas;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {      
        atualiza();
        readObject();     
    }    
    private String verificarFormaPagemento(){
        if(cartao.isSelected()){
            return "Cartão";
        }else if(boleto.isSelected()){
            return "Boleto";
        }
        return null;
    }
    private boolean verificarEstoque(Produto p){
        if(p.getEstoque() >= Integer.parseInt(quantidadeTF.getText())){
            return true;
        }else{
            return false;
        }
    }
    private boolean verificarEstoque(Produto p, int quantidade){
        if(p.getEstoque() >= quantidade){
            return true;
        }else{
            return false;
        }
    }
    
    @FXML
    private void addAoCarrinho(ActionEvent event) {
         if(verificarEstoque((Produto) produtoCB.getSelectionModel().getSelectedItem())){
            ObjetoCarrinho oc = new ObjetoCarrinho();
            Produto p = new Produto();
            p = (Produto) produtoCB.getSelectionModel().getSelectedItem();
            oc.setProduto(p);
            oc.setQuantidade(Integer.parseInt(quantidadeTF.getText()));
            oc.setValorUnitario(p.getValor());
            oc.setValorTotal();
            objetosNoCarrinho.add(oc);
            g.escreve(oc);
         }else{
             msgSistema.setText("Estoque insuficiente!!!");
         }
    }

    @FXML
    private void finalizarCompra(ActionEvent event) {
        int auxEstoque = 0;
        if(objetosNoCarrinho.isEmpty()){
            msgSistema.setText("Selecione pelo menos um item antes de finalizar a compra!");
        }else{
            quantidadeDaCompra = 0;
            for(ObjetoCarrinho oc: objetosNoCarrinho){
                quantidadeDaCompra += oc.getQuantidade();
            }
            //if(verificarEstoque((Produto) produtoCB.getSelectionModel().getSelectedItem(), quantidadeDaCompra)){
            //if(true){
                Venda v = new Venda();
                Double precoTotal = 0.0;
                v.setCliente(Trabalho3Trimestre.cliente);
                java.util.Date dataAtual = new java.util.Date(); 
                java.sql.Date dataSQL = new java.sql.Date(dataAtual.getTime());
                v.setData(dataSQL);
                v.setFormaPagamento(verificarFormaPagemento());
                for(ObjetoCarrinho oc: objetosNoCarrinho){
                    precoTotal+= oc.getValorTotal();
                    Produto p = oc.getProduto();
                    p.setEstoque(p.getEstoque() - oc.getQuantidade());
                    p.update();
                }
                v.setPreco(precoTotal);
                //v.inserir();
                v.inserirVenda();
                for(ObjetoCarrinho oc: objetosNoCarrinho){
                    ItemVenda iv = new ItemVenda();
                    iv.setIdVenda(v.getId());
                    iv.setIdProduto(oc.getProduto().getId());
                    iv.setValor(oc.getValorTotal());
                    iv.setQuantidade(oc.getQuantidade());
                    iv.inserir();
                }
                objetosNoCarrinho.clear();
                atualiza();
           /* }else{
                msgSistema.setText("Algum de seus itens esta sem estoque!!!");
                objetosNoCarrinho.clear();            
            }*/
        }
    }
    public void atualiza(){
        vendas = tabelaVendas.getItems(); 
        vendas.clear();        
        idVendasCol.setCellValueFactory(new PropertyValueFactory<>("id"));      
        dataVendasCol.setCellValueFactory(new PropertyValueFactory<>("data")); 
        formaPagamentoVendasCol.setCellValueFactory(new PropertyValueFactory<>("formaPagamento"));
        clienteVendasCol.setCellValueFactory(new PropertyValueFactory<>("cliente"));
        valorTotalVendasCol.setCellValueFactory(new PropertyValueFactory<>("preco"));
        
        objetosNoCarrinho = tabelaCarrinho.getItems(); 
        objetosNoCarrinho.clear();        
        nomeCarrinhoCol.setCellValueFactory(new PropertyValueFactory<>("produto"));      
        valorCarrinhoCol.setCellValueFactory(new PropertyValueFactory<>("valorUnitario")); 
        valorTotalCarrinhoCol.setCellValueFactory(new PropertyValueFactory<>("valorTotal"));
        quantidadeCarrinhoCol.setCellValueFactory(new PropertyValueFactory<>("quantidade"));
        
        ArrayList<Venda> vendasBanco = Venda.getAll();
        for(Venda v: vendasBanco){
            if(v.getCliente().getLogin().equals(Trabalho3Trimestre.cliente.getLogin())){
                    vendas.add(v);
            }
        }
        
        produtoCB.getItems().clear();
        ArrayList<Produto> produtos = Produto.getAll();
        for(Produto prod: produtos){
            if(prod.getEstoque()>0){
                produtoCB.getItems().add(prod);
            }
        }
    }
    
    public void readObject(){
        
  
       try{
           ArrayList<ObjetoCarrinho> aux = new ArrayList<>();
            
           FileInputStream fin = new FileInputStream("objetoscarrinho.ser");

           ObjectInputStream ois = new ObjectInputStream(fin);
            
           aux = (ArrayList<ObjetoCarrinho>) ois.readObject();
           ois.close();
           for(ObjetoCarrinho oc: aux){
               TelaVendasController.objetosNoCarrinho.add(oc);
           }
  
           System.out.println(TelaVendasController.objetosNoCarrinho);
  
       }catch(Exception ex){
           ex.printStackTrace(); 
       } 
   }
}
