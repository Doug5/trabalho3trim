/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.douglassilvadelima.model;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Aluno
 */
public class ObjetoCarrinho implements Serializable{
    private Produto produto;
    private double valorUnitario;
    private double valorTotal;
    private int quantidade;

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    

    public double getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal() {
        this.valorTotal = this.quantidade*this.valorUnitario;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }   
    
    @Override
       public String toString() {
           return new StringBuffer(" ObjetoCarrinho : ")
           .append(this.produto)
           .append(" VU : ")
           .append(this.valorUnitario)
           .append(" VT : ")
           .append(this.valorTotal)
           .append(" Quantidade : ")
           .append(this.quantidade).toString();
       }
}
