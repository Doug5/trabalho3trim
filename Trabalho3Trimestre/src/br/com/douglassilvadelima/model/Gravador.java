/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.douglassilvadelima.model;

import br.com.douglassilvadelima.view.Trabalho3Trimestre;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;

/**
 *
 * @author marcio
 */
public class Gravador {

    private File f;
    private FileWriter fw;

    public Gravador() {
        f = new File("carrinho.txt");      
        try {            
            fw = new FileWriter(f);
            fw.write("Arquivo gravado em : " + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void escreve(ObjetoCarrinho oc) {
        try {
             fw.append("\r\n Cliente: " + Trabalho3Trimestre.cliente.getLogin() + " adicionou o produto: " + oc.getProduto().getNome() + " (x " + oc.getQuantidade() + ")");
             fw.flush();//Envia os dados para o arquivo
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            fw.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        super.finalize(); 
    }
}

