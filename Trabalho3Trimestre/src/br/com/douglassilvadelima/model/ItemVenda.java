/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.douglassilvadelima.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Aluno
 */
public class ItemVenda {
    private int idVenda;
    private int idProduto;
    private double valor;
    private int quantidade;

    public int getIdVenda() {
        return idVenda;
    }

    public void setIdVenda(int idVenda) {
        this.idVenda = idVenda;
    }

    public int getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }


    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
    
    
    
    public void inserir(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        
        String insertTableSQL = "INSERT INTO Item_vendaTT3(id_venda, id_produto, valor, quantidade) VALUES( ?, ?, ?, ?) ";
        
        try{
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            
            preparedStatement.setInt(1, this.idVenda);
            preparedStatement.setInt(2, this.idProduto);
            preparedStatement.setDouble(3, this.valor);
            preparedStatement.setInt(4, this.quantidade);
            
            preparedStatement.executeUpdate();

            System.out.println("Record is inserted into ItemVenda table!");
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }       
    }
}
