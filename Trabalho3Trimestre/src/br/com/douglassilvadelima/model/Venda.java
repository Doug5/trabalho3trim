/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.douglassilvadelima.model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class Venda {
    private int id;
    private Date data;
    private String formaPagamento;
    private Cliente cliente;
    private Double preco;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(String formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Double getPreco() {
        return preco;
    }

    public void setPreco(Double preco) {
        this.preco = preco;
    }
    
    
    public void inserir(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        
        String insertTableSQL = "INSERT INTO VendaTT3(id_venda, data, forma_pagamento, preco, id_cliente) VALUES( seq_venda_id.nextval, ?, ?, ?, ?) ";
        
        try{
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            
            preparedStatement.setDate(1, this.data);
            preparedStatement.setString(2, this.formaPagamento);
            preparedStatement.setDouble(3, this.preco);
            preparedStatement.setInt(4, this.cliente.getId());
            
            preparedStatement.executeUpdate();

            System.out.println("Record is inserted into Venda table!");
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }       
    }
    
    public int inserirVenda(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        int chaveGerada = 0;
        String insertTableSQL = "INSERT INTO VendaTT3(id_venda, data, forma_pagamento, preco, id_cliente) VALUES( seq_venda_id.nextval, ?, ?, ?, ?) ";
        
        try{
             String generatedColumns[] = {"id_venda"};
            
            preparedStatement = dbConnection.prepareStatement(insertTableSQL, generatedColumns);
            
            Statement st = dbConnection.createStatement();
            
            preparedStatement.setDate(1, this.data);
            preparedStatement.setString(2, this.formaPagamento);
            preparedStatement.setDouble(3, this.preco);
            preparedStatement.setInt(4, this.cliente.getId());
            
            preparedStatement.executeUpdate();

            System.out.println("Record is inserted into Venda table!");
            ResultSet rs = preparedStatement.getGeneratedKeys();
            
            while (rs.next()) {
                chaveGerada = rs.getInt(1);
                System.out.println("id gerado: " + chaveGerada);
            }
            this.setId(chaveGerada);
            
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return chaveGerada;
    }
    
    public static ArrayList<Venda> getAll(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<Venda> lista = new ArrayList<>();
        String selectSQL = "SELECT * FROM VendaTT3";
        Statement st;
        try{
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                Venda v = new Venda();
                v.setId(rs.getInt("id_venda"));
                v.setData(rs.getDate("data"));
                v.setPreco(rs.getDouble("preco"));
                v.setFormaPagamento(rs.getString("forma_pagamento"));
                Cliente cliente = new Cliente();
                cliente.setId(rs.getInt("id_cliente"));
                cliente.getOne();
                v.setCliente(cliente);
                lista.add(v);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return lista;
    }
    
    public void update(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps;
        
        String updateTableSQL = "UPDATE VendaTT3 set data = ?, forma_pagamento = ?, preco = ?, id_cliente = ? where id_venda = ?";
        try{
            ps = dbConnection.prepareStatement(updateTableSQL);
            
            ps.setDate(1, this.data);
            ps.setString(2, this.formaPagamento);
            ps.setDouble(3, this.preco);
            ps.setInt(4, this.cliente.getId());
            ps.setInt(5, this.id);
            
            ps.executeUpdate();
            System.out.println("Venda alterada!");
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
    }
}
