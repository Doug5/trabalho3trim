/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.douglassilvadelima.model;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class Raca implements Serializable{
    private int id;
    private String nome;
    private String epoca;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEpoca() {
        return epoca;
    }

    public void setEpoca(String epoca) {
        this.epoca = epoca;
    }

    @Override
    public String toString() {
        return nome;
    }
    
    public void inserir(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        
        String insertTableSQL = "INSERT INTO RacaTT3(id_raca, nome, epoca) VALUES( seq_raca_id.nextval, ?, ?)";
        try{
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            
            preparedStatement.setString(1, this.nome);
            preparedStatement.setString(2, this.epoca);
            
            preparedStatement.executeUpdate();

            System.out.println("Record is inserted into Raca table!");
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }       
    }
    
    public void getOne(int i){
        this.id = i;
        getOne();
    }
    public void getOne(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        String selectSQL = "SELECT * FROM RacaTT3 WHERE id_raca = ?";
        
        PreparedStatement ps;
        try{
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.id);
            
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                this.setId(rs.getInt("id_raca"));
                this.setNome(rs.getString("nome"));
                this.setEpoca(rs.getString("epoca"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
    }
    
    public static ArrayList<Raca> getAll(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<Raca> lista = new ArrayList<>();
        String selectSQL = "SELECT * FROM RacaTT3";
        Statement st;
        try{
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                Raca r = new Raca();
                r.setId(rs.getInt("id_raca"));
                r.setNome(rs.getString("nome"));
                r.setEpoca(rs.getString("epoca"));
                lista.add(r);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return lista;
    }

    
}
