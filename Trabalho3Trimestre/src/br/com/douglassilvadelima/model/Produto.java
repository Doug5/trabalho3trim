/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.douglassilvadelima.model;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class Produto implements Serializable{
    private int id;
    private String nome;
    private double valor;
    private int estoque;
    private Raca raca;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getEstoque() {
        return estoque;
    }

    public void setEstoque(int estoque) {
        this.estoque = estoque;
    }

    public Raca getRaca() {
        return raca;
    }

    public void setRaca(Raca raca) {
        this.raca = raca;
    }
    
    public void inserir(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        
        String insertTableSQL = "INSERT INTO ProdutoTT3(id_produto, nome, valor, estoque, id_raca) VALUES( seq_produto_id.nextval, ?, ?, ?, ?)";
        try{
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            
            preparedStatement.setString(1, this.nome);
            preparedStatement.setDouble(2, this.valor);
            preparedStatement.setInt(3, this.estoque);
            preparedStatement.setInt(4, this.raca.getId());
            
            preparedStatement.executeUpdate();

            System.out.println("Record is inserted into Produto table!");
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }       
    }
    
    public static ArrayList<Produto> getAll(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<Produto> lista = new ArrayList<>();
        String selectSQL = "SELECT * FROM ProdutoTT3";
        Statement st;
        try{
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                Produto p = new Produto();
                p.setId(rs.getInt("id_produto"));
                p.setNome(rs.getString("nome"));
                p.setValor(rs.getDouble("valor"));
                p.setEstoque(rs.getInt("estoque"));
                Raca r = new Raca();
                r.setId(rs.getInt("id_raca"));
                r.getOne();
                p.setRaca(r);
                lista.add(p);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return lista;
    }
    
    public void getOne(int i){
        this.id = i;
        getOne();
    }
    public void getOne(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        String selectSQL = "SELECT * FROM ProdutoTT3 WHERE id_produto = ?";
        
        PreparedStatement ps;
        try{
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.id);
            
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                this.setId(rs.getInt("id_produto"));
                this.setNome(rs.getString("nome"));
                this.setValor(rs.getDouble("valor"));
                this.setEstoque(rs.getInt("estoque"));
                Raca r = new Raca();
                r.setId(rs.getInt("id_raca"));
                r.getOne();
                this.setRaca(r);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
    }
    
    public void update(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps;
        
        String updateTableSQL = "UPDATE ProdutoTT3 set nome = ?, valor = ?, estoque = ?, id_raca = ? where id_produto = ?";
        try{
            ps = dbConnection.prepareStatement(updateTableSQL);
            
            ps.setString(1, this.nome);
            ps.setDouble(2, this.valor);
            ps.setInt(3, this.estoque);
            ps.setInt(4, this.raca.getId());
            ps.setInt(5, this.id);
            
            ps.executeUpdate();
            System.out.println("Familia alterada para " + this.nome + "!");
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
    }

    @Override
    public String toString() {
        return nome;
    }



}
