
/* Drop Triggers */

DROP TRIGGER TRI_clienteTT3_id_cliente;
DROP TRIGGER TRI_cliente_id;
DROP TRIGGER TRI_cliente_id_cliente;
DROP TRIGGER TRI_produtoTT3_id_produto;
DROP TRIGGER TRI_produto_id;
DROP TRIGGER TRI_produto_id_produto;
DROP TRIGGER TRI_racaTT3_id_raca;
DROP TRIGGER TRI_raca_id;
DROP TRIGGER TRI_raca_id_raca;
DROP TRIGGER TRI_vendaTT3_id_venda;
DROP TRIGGER TRI_venda_id;
DROP TRIGGER TRI_venda_id_venda;



/* Drop Tables */

DROP TABLE item_vendaTT3 CASCADE CONSTRAINTS;
DROP TABLE vendaTT3 CASCADE CONSTRAINTS;
DROP TABLE clienteTT3 CASCADE CONSTRAINTS;
DROP TABLE produtoTT3 CASCADE CONSTRAINTS;
DROP TABLE racaTT3 CASCADE CONSTRAINTS;



/* Drop Sequences */

DROP SEQUENCE SEQ_clienteTT3_id_cliente;
DROP SEQUENCE SEQ_cliente_id;
DROP SEQUENCE SEQ_cliente_id_cliente;
DROP SEQUENCE SEQ_produtoTT3_id_produto;
DROP SEQUENCE SEQ_produto_id;
DROP SEQUENCE SEQ_produto_id_produto;
DROP SEQUENCE SEQ_racaTT3_id_raca;
DROP SEQUENCE SEQ_raca_id;
DROP SEQUENCE SEQ_raca_id_raca;
DROP SEQUENCE SEQ_vendaTT3_id_venda;
DROP SEQUENCE SEQ_venda_id;
DROP SEQUENCE SEQ_venda_id_venda;




/* Create Sequences */

CREATE SEQUENCE SEQ_clienteTT3_id_cliente INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_cliente_id INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_cliente_id_cliente INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_produtoTT3_id_produto INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_produto_id INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_produto_id_produto INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_racaTT3_id_raca INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_raca_id INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_raca_id_raca INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_vendaTT3_id_venda INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_venda_id INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_venda_id_venda INCREMENT BY 1 START WITH 1;



/* Create Tables */

CREATE TABLE clienteTT3
(
	id_cliente number NOT NULL,
	nome varchar2(50),
	login varchar2(50) NOT NULL,
	senha varchar2(50),
	PRIMARY KEY (id_cliente)
);


CREATE TABLE item_vendaTT3
(
	id_produto number NOT NULL,
	id_venda number NOT NULL,
	valor number
);


CREATE TABLE produtoTT3
(
	id_produto number NOT NULL,
	nome varchar2(200),
	valor number,
	estoque number,
	id_raca number NOT NULL,
	PRIMARY KEY (id_produto)
);


CREATE TABLE racaTT3
(
	id_raca number NOT NULL,
	nome varchar2(50),
	epoca varchar2(50),
	PRIMARY KEY (id_raca)
);


CREATE TABLE vendaTT3
(
	id_venda number NOT NULL,
	data date,
	forma_pagamento varchar2(50),
	id_cliente number NOT NULL,
	PRIMARY KEY (id_venda)
);



/* Create Foreign Keys */

ALTER TABLE vendaTT3
	ADD FOREIGN KEY (id_cliente)
	REFERENCES clienteTT3 (id_cliente)
;


ALTER TABLE item_vendaTT3
	ADD FOREIGN KEY (id_produto)
	REFERENCES produtoTT3 (id_produto)
;


ALTER TABLE produtoTT3
	ADD FOREIGN KEY (id_raca)
	REFERENCES racaTT3 (id_raca)
;


ALTER TABLE item_vendaTT3
	ADD FOREIGN KEY (id_venda)
	REFERENCES vendaTT3 (id_venda)
;



/* Create Triggers */

CREATE OR REPLACE TRIGGER TRI_clienteTT3_id_cliente BEFORE INSERT ON clienteTT3
FOR EACH ROW
BEGIN
	SELECT SEQ_clienteTT3_id_cliente.nextval
	INTO :new.id_cliente
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_cliente_id BEFORE INSERT ON cliente
FOR EACH ROW
BEGIN
	SELECT SEQ_cliente_id.nextval
	INTO :new.id
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_cliente_id_cliente BEFORE INSERT ON cliente
FOR EACH ROW
BEGIN
	SELECT SEQ_cliente_id_cliente.nextval
	INTO :new.id_cliente
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_produtoTT3_id_produto BEFORE INSERT ON produtoTT3
FOR EACH ROW
BEGIN
	SELECT SEQ_produtoTT3_id_produto.nextval
	INTO :new.id_produto
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_produto_id BEFORE INSERT ON produto
FOR EACH ROW
BEGIN
	SELECT SEQ_produto_id.nextval
	INTO :new.id
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_produto_id_produto BEFORE INSERT ON produto
FOR EACH ROW
BEGIN
	SELECT SEQ_produto_id_produto.nextval
	INTO :new.id_produto
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_racaTT3_id_raca BEFORE INSERT ON racaTT3
FOR EACH ROW
BEGIN
	SELECT SEQ_racaTT3_id_raca.nextval
	INTO :new.id_raca
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_raca_id BEFORE INSERT ON raca
FOR EACH ROW
BEGIN
	SELECT SEQ_raca_id.nextval
	INTO :new.id
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_raca_id_raca BEFORE INSERT ON raca
FOR EACH ROW
BEGIN
	SELECT SEQ_raca_id_raca.nextval
	INTO :new.id_raca
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_vendaTT3_id_venda BEFORE INSERT ON vendaTT3
FOR EACH ROW
BEGIN
	SELECT SEQ_vendaTT3_id_venda.nextval
	INTO :new.id_venda
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_venda_id BEFORE INSERT ON venda
FOR EACH ROW
BEGIN
	SELECT SEQ_venda_id.nextval
	INTO :new.id
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_venda_id_venda BEFORE INSERT ON venda
FOR EACH ROW
BEGIN
	SELECT SEQ_venda_id_venda.nextval
	INTO :new.id_venda
	FROM dual;
END;

/




